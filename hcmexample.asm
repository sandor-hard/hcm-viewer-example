;	HARD Color Map "HCM" Example
;	Created by Sandor Teli / HARD (10/2016-11/2017)
;
;	requirements: 64K PAL XL/XE
;
;	Note: This is a simple example demonstrating how to
;	display HCM mode 0 and 2 images at a basic level.
;	For more advanced topics like transitioning between
;	HCM images; assembling & dismantling HCM images;
;	driving HARDBASS from within the HCM display kernel
;	see the REHARDEN demo and its source code repo

	ICL "equates.asm"

	hcm_file_mode_descriptor_offset = 6
	hcm_file_palette_colors_offset = 7
	hcm_file_pic_data_start_offset = $800
	
	color_bck = 0
	color_underlay1 = 1
	color_underlay2 = 2
	color_playfield0 = 3
	color_playfield1 = 4
	color_playfield2 = 5

	dma_setting = 1+4+8+16+32

	org $1ff0 ;starting $10 bytes early as the HCM file header is $10 bytes long

hcm_file_header

	;switch between pictures here (check the source folder to see the example files available)
	INS "tut2.hcm"

	picdata = hcm_file_header+$10

	;in this simple example, we will point our DLIST right at the picture/sprite data we loaded
	;see REHARDEN on a more complex use case where multiple pictures share the same display space
	sprite_start = picdata
	sprite_replica_start = sprite_start
	display = sprite_start+$800

dlist_start
	.ds $3f0 ;you can re-use this dlist across images (see REHARDEN and how it copies various pictures into the same display space)

start	sei

	lda #$00
	sta NMIEN
	sta IRQEN

	lda #<display
	sta dlist_scr_ptr
	lda #>display
	sta dlist_scr_ptr+1

	ldx #$c0
	lda #<dlist_start
	sta dlist_ptr+1
	lda #>dlist_start
	sta dlist_ptr+2
	lda #<dlist_start
	sta dljmplo+1
	lda #>dlist_start
	sta dljmphi+1
	jsr initdlist

	jsr waitforvblank

	lda #dma_setting
	sta DMACTL

	jsr initsprites
	jsr inithcmmode

	lda #<dlist_start
	sta DLISTL
	lda #>dlist_start
	sta DLISTH


loop	

	lda #$0f
	jsr waitforvcount

	tsx
	stx spsave
	
	ldx #$00
	txs

	sta WSYNC
	
lineloop
	nop ;use these 4 cycles for whatever you like
	nop ;REHARDEN uses this space to drive the HARDBASS soft-synth

	;This example with the repeat statement is a little bit more complex
	;than the absolution minimum would be - as you could get away with doing a loop instead.
	;The reason I included this solution here is because in a real-world product you might want
	;to drive HARDBASS or do other small maintenance tasks in your display kernel.
	;...and breaking up the display kernel into blocks of 8 scanlines helps with that challenge.
	;See how REHARDEN drives HARDBASS using this method.
 
	.rept 8, #
	.if :1==0
	;can spend 0 cycles in here (1st line of the group of 8 scanlines)
	.else
	;can spend 11 cycles in here (any other line than the 1st one)
	nop ; use it for whatever you like
	nop
	nop
	nop
	cmp $ff ;just wasting 3 cycles of the 11, no op
	.endif
	
	sta WSYNC

	lda #$48 ;hpos
sthpos1_1_:1
	sta HPOSP2
	sta HPOSP0

	.if :1==0
	nop ;placeholder showing how you can spend 6 cycles
	nop ;doing different tasks each line of the group of 8 scanlines
	nop ;REHARDEN uses this space to drive the HARDBASS soft-synth
	.elseif :1==1
	nop ;placeholder showing how you can spend 6 cycles
	nop ;doing different tasks each line of the group of 8 scanlines
	nop ;REHARDEN uses this space to drive the HARDBASS soft-synth
	.else
	nop ;placeholder showing how you can spend 6 cycles
	nop ;doing different tasks each line of the group of 8 scanlines
	nop
	.endif

	ldy sprite_replica_start+$20, x
	lda sprite_replica_start+$120, x
	tax

	lda #$98
	sta HPOSP0
	sty GRAFP0
sthpos1_2_:1
	sta HPOSP2
stgraf1_:1
	stx GRAFP2
	
	tsx
	inx
	txs
	.endr

	cpx #$c0
	beq afterhcm

	jmp lineloop

afterhcm	
	ldx spsave
	txs

	jmp loop

waitforvblank
	lda #$7c
waitforvcount
	cmp VCOUNT
	bne waitforvcount
	rts

inithcmmode

	lda hcm_file_header+hcm_file_mode_descriptor_offset
	beq inithcmmode_mixpmpf
	cmp #$02
	bne inithcmmode_unknown
	jmp inithcmmode_dontmixpmpf

inithcmmode_unknown
	brk ;only mode 0 and 2 are supported in this example

inithcmmode_mixpmpf
	lda #$00
	sta PRIOR

	;mixing PM colors with PF colors in a fully homogenous way requires
	;a certain positioning of PMs

	lda #$48
	sta HPOSP0
	sta HPOSP2
	lda #$68
	sta HPOSP1
	sta HPOSP3
	lda #$88
	sta HPOSM0
	sta HPOSM2
	lda #$90
	sta HPOSM1
	sta HPOSM3

	lda #<HPOSP2
	.rept 8, #
	sta sthpos1_1_:1+1
	sta sthpos1_2_:1+1
	.endr
	lda #<GRAFP2
	.rept 8, #
	sta stgraf1_:1+1
	.endr

	lda hcm_file_header+hcm_file_palette_colors_offset+color_bck
	sta COLBK
	
	lda hcm_file_header+hcm_file_palette_colors_offset+color_underlay1
	sta COLPM0
	sta COLPM1
	
	lda hcm_file_header+hcm_file_palette_colors_offset+color_underlay2
	sta COLPM2
	sta COLPM3
	
	lda hcm_file_header+hcm_file_palette_colors_offset+color_playfield0
	sta COLPF0
	lda hcm_file_header+hcm_file_palette_colors_offset+color_playfield1
	sta COLPF1
	lda hcm_file_header+hcm_file_palette_colors_offset+color_playfield2
	sta COLPF2

	rts

inithcmmode_dontmixpmpf
	lda #$24
	sta PRIOR

	;mixing PM colors with each other in a fully homogenous way requires
	;a certain positioning of PMs (different from mode 0 above)

	lda #$48
	sta HPOSP0
	sta HPOSP1
	lda #$68
	sta HPOSP2
	sta HPOSP3
	lda #$88
	sta HPOSM0
	sta HPOSM1
	lda #$90
	sta HPOSM2
	sta HPOSM3

	lda #<HPOSP1
	.rept 8, #
	sta sthpos1_1_:1+1
	sta sthpos1_2_:1+1
	.endr
	lda #<GRAFP1
	.rept 8, #
	sta stgraf1_:1+1
	.endr

	lda hcm_file_header+hcm_file_palette_colors_offset+color_bck
	sta COLBK
	
	lda hcm_file_header+hcm_file_palette_colors_offset+color_underlay1
	sta COLPM0
	sta COLPM2
	
	lda hcm_file_header+hcm_file_palette_colors_offset+color_underlay2
	sta COLPM1
	sta COLPM3
	
	lda hcm_file_header+hcm_file_palette_colors_offset+color_playfield0
	sta COLPF0
	lda hcm_file_header+hcm_file_palette_colors_offset+color_playfield1
	sta COLPF1
	lda hcm_file_header+hcm_file_palette_colors_offset+color_playfield2
	sta COLPF2

	rts

	;you could generate interlaced DLISTs here as well for the interlaced HCM modes,
	;but that's outside of the scope of this example as:
	;- very few people use CRTs today (unfortunately.. - but I still do!)
	;- emulators make interlaced modes look way too good
	;  (and unrealistically good looking results is not what I'm after)
initdlist
	;header
	lda #$70
	jsr store_dl_byte
	jsr store_dl_byte
	jsr store_dl_byte

dlloop	lda #$4e
	jsr store_dl_byte
	lda dlist_scr_ptr
	jsr store_dl_byte
	clc
	adc #$20
	sta dlist_scr_ptr
	lda dlist_scr_ptr+1
	jsr store_dl_byte
	adc #$00
	sta dlist_scr_ptr+1
	dex
	bne dlloop
	
	;footer
	lda #$41
	jsr store_dl_byte
dljmplo	lda #$00
	jsr store_dl_byte
dljmphi	lda #$00
	jsr store_dl_byte
		
	rts
	
store_dl_byte
dlist_ptr
	sta dlist_start
	inc dlist_ptr+1
	bne str_dle
	inc dlist_ptr+2
str_dle	rts

read_dl_byte
	lda dlist_ptr+1
	sta rdbptr+1
	lda dlist_ptr+2
	sta rdbptr+2
rdbptr	lda $ffff
	rts

dlist_scr_ptr
	.byte <display,>display

initsprites
	lda #>sprite_start
	sta PMBASE
	
	lda #$00
	sta PRIOR

	ldy #$07
	lda #$ff
ispr0	sta HPOSP0,y
	dey
	bpl ispr0

	lda #$03
	sta SIZEP0
	sta SIZEP1
	sta SIZEP2
	sta SIZEP3
	lda #$ff
	sta SIZEM

	lda #$03
	sta GRACTL
	
	rts

	run start

	org $80
spsave	.ds 1


