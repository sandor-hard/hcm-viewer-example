# Hard Color Map Viewer Example

## What does this repo include?

This repo includes the full source code of a HCM Viewer example project for the Atari XL/XE.
Use the [WUDSN IDE](https://www.wudsn.com/) to compile and run hcmexample.asm.

Please click the link below for details on the HARD Color Map graphics format:

- [Hard Color Map a.k.a. HCM documentation](https://bitbucket.org/sandor-hard/reharden/src/master/HCM.md)
- [HCM Converter](https://bitbucket.org/sandor-hard/hcm-converter/)

Sandor Teli / HARD
